<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class,'index'] );


Route::post('/addSong', [Controller::class,'addSong'] )->name('addSong');
Route::post('/removeSong', [Controller::class,'removeSong'])->name('removeSong');
Route::post('/updateQueue', [Controller::class, 'updateQueue'])->name('updateQueue');
Route::get('/queue', [Controller::class,'queue'] )->name('getQueue');

//Route::get('/{date}', [Controller::class, 'show'] )->name('show');
