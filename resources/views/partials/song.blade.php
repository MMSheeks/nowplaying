<div class="row song pt-2" id="{{ $song->id }}" data-queueposition="{{ $song->queue_position }}" data-title="{{ $song->title }}" data-artist="{{ $song->artist }}" data-album="{{ $song->album }}">
    <div class="col-4">
        <p>
            <i class="fas fa-bars dragger align-middle mr-2"></i>
            <i class="far fa-times-circle removeSong align-middle mr-2" data-id="{{ $song->id }}"></i>
            {{ $song->title }}</p>
    </div>
    <div class="col-3">
        <p>{{ $song->artist }}</p>
    </div>
    <div class="col-3">
        <p>{{ $song->album }}</p>
    </div>
    <div class="col-2">
        @if( filter_var( $song->source, FILTER_VALIDATE_URL ) )
            <a href="{{ $song->source }}">Link</a>
        @else
            <p>{{ $song->source }}</p>
        @endif
        <span class="nextSong">Next Up</span>
    </div>
</div>
