<div class="playQueue">
    <div class="row" id="queueHeader">
        <div class="col-4">
            <h3>Title</h3>
        </div>
        <div class="col-3">
            <h3>Artist</h3>
        </div>
        <div class="col-3">
            <h3>Album</h3>
        </div>
        <div class="col-2">
            <h3>Source</h3>
        </div>
    </div>
    <div id="songList">
        @foreach( $songs as $song )
            @include('partials.song')
        @endforeach
    </div>
</div>
