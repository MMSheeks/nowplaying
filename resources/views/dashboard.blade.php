<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/0618b3ca5e.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="{{ asset('style.css') }}">

        <title>Now Playing</title>
    </head>
    <body>
        <div class="fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        <img class="logo" src="{{ asset('logo.jpg' ) }}"/>
                        <h1 class="pt-4">Now Playing</h1>
                    </div>
                    <div class="col-4 text-right">
                        <button class="btn btn-default mt-4 btn-lg" id="go">Go</button>
                        <button class="btn btn-secondary mt-4 btn-lg" id="back">Back</button>
                        <button class="btn btn-secondary mt-4 btn-lg" id="clear">Clear</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-4">
                <div class="col-12" id="queue">
                    @include('partials.queue')
                </div>
            </div>
            <div class="fixed-bottom" id="addSongTray">
                <div class="container">
                    <h2>Add Song</h2>
                    <form class="form-inline" id="addSongForm">
                        <label class="sr-only" for="songTitle">Title</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" id="songTitle" name="songTitle" placeholder="Song Title">
                        <label class="sr-only" for="songArtist">Artist</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" id="songArtist" name="songArtist" placeholder="Artist">
                        <label class="sr-only" for="songAlbum">Album</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" id="songAlbum" name="songAlbum" placeholder="Album">
                        <label class="sr-only" for="songSource">Source</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" id="songSource" name="songSource" placeholder="Source">
                        <label class="sr-only" for="showNight">Night</label>
                        <input type="date" class="form-control mb-2 mr-sm-2" id="showNight" name="showNight" value="<?php echo date('Y-m-d', strtotime('this Friday') ) ?>">
                        <button type="submit" class="form-control btn btn-default mb-2" id="addSong">Add</button>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
        <script>
            const urls = {
                _token: "{{ csrf_token() }}",
                addSong: "{{ route('addSong' ) }}",
                getQueue: "{{ route('getQueue' ) }}",
                removeSong: "{{ route('removeSong') }}",
                updateQueuePosition: "{{ route('updateQueue') }}"
            }
            var minQueue = {{ $min_queue }};
        </script>
        <script src="{{ asset('script.js') }}"></script>
    </body>
</html>
