<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NowplayingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function( Blueprint $t ) {
            $t->id();
            $t->string('title');
            $t->string('artist');
            $t->string('album');
            $t->string('source');
            $t->integer('queue_position');
            $t->date('show_night');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
