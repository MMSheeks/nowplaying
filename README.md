## Now Playing Song Queue App

This is an open source project to make a simple app to display a song from a queue on a livestream web source. The code here is not heavily tested and not particularly clean, use at your own risk.

### Requirements

#### Web App
- PHP7+
- MySQL or PostgreSQL

#### Node App
- Node.js

### Set Up

#### Web App
- Clone this repository to your server
- run `composer install`
- run `php artisan migrate`
- Navigate to the URL for your web server and make sure everything's working
- Troubleshoot as necessary

#### Node.JS App
- Clone this repository to your server (in a new folder, if not a separate server from the web app)
- Checkout the node-app branch
- Start a screen session (or multiple terminal app of your choice)
- Run `node index.js`
- Check the URL for your application at port 3000 to make sure it loads correctly
- In the Web App, update the code to point to the correct instance of this application. Currently hard coded to my server IP.

#### StreamLabs OBS (or your streaming platform of choice)
- Add the URL of your Node.js app (with the port) as a web source layer to your stream

### Usage
- Add songs to the queue in the web app
- use the "next" and "back" buttons to move the displayed song forwards or backwards on the live stream layer
- use the "clear" button to clear the text on the live stream layer