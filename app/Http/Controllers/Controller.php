<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

use App\Models\Song;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $date;

    public function __construct() {
        $this->date = $this->getShowDate();
    }

    public function index() {
        $songs = $this->tonightsSongs();
        $minQueue = $this->minQueue();
        return view('dashboard', [ 'songs' => $songs, 'min_queue' => $minQueue ]);
    }

    public function addSong( Request $req ) {

        // get last known queue position
        $lastQueuePosition = Song::max('queue_position');
        if( $lastQueuePosition == null ) {
            $lastQueuePosition = 0;
        }

        $song = new Song;
        $song->title = $req->title;
        $song->artist = $req->artist;
        $song->album = $req->album;
        $song->source = $req->source;
        $song->show_night = date('Y-m-d', strtotime( $req->date ) );
        $song->queue_position = $lastQueuePosition +1;
        $song->save();

        return response()->json( $song );
    }

    public function queue() {
        $songs = $this->tonightsSongs();
        return view('partials.queue', ['songs' => $songs ])->render();
    }

    public function removeSong( Request $req ) {
        $song = Song::find( $req->id );
        $queuePosition = $song->queue_position;
        $song->delete();

        // now update the queue position of all songs above it.
        $songs = Song::where('queue_position', '>', $queuePosition)->get();
        foreach( $songs as $song ) {
            $song->queue_position = $song->queue_position -1;
            $song->save();
        }

        return response()->json( 'success' );
    }

    public function updateQueue( Request $req )
    {
        $modifying = Song::find( $req->song );
        $start = $this->minQueue();
        foreach( $req->order as $id ) {
            $song = Song::find($id);
            $song->queue_position = $start;
            $song->save();
            $start++;
        }

        return response()->json('success');
    }

    public function tonightsSongs() {
        $songs = Song::where( 'show_night', $this->date )->orderBy('queue_position')->get();
        //$songs = Song::all();
        return $songs;
    }

    public function minQueue() {
        $minQueue = Song::where( 'show_night', $this->date )->min('queue_position');
        if( $minQueue == null ) {
            $minQueue = 0;
        }
        return $minQueue;
    }

    public function getShowDate() {
        $time = date('G');
        if( $time > 1 ) {
            return date('Y-m-d', strtotime('yesterday') );
        } else {
            return date('Y-m-d');
        }
    }
}
