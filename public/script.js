$.fn.filterByData = function(prop, val) {
    return this.filter(
        function() { return $(this).data(prop)==val; }
    );
}

const dashboard = {
    queuePosition: 0,
    date: $("#nextShow").val(),
    title: "",
    artist: "",
    album: "",
    storage: window.localStorage,

    init: function() {
        dashboard.setPositionZero();
        dashboard.broadcaster.init();
        $(document).on('submit', '#addSongForm', dashboard.addSong );
        $(document).on('click', '#go', dashboard.go );
        $(document).on('click', '#clear', dashboard.broadcaster.clearMsg );
        $(document).on('click', '#back', dashboard.back );
        $(document).on('click', '.removeSong', dashboard.removeSong );
        dashboard.initSortable();
    },

    initSortable: function() {
        $("#songList").sortable({
            handle: '.dragger',
            animation:100,
            onEnd: dashboard.reorder
        });
    },

    go: function() {
        dashboard.queuePosition++;
        dashboard.storage.setItem('queuePosition', dashboard.queuePosition);

        dashboard.setCurrentSong();
        dashboard.setNextSong();
        dashboard.broadcaster.setSong();
    },

    back: function() {
        dashboard.queuePosition--;
        dashboard.storage.setItem('queuePosition', dashboard.queuePosition);

        dashboard.setCurrentSong();
        dashboard.setNextSong();
        dashboard.broadcaster.setSong();
    },

    addSong: function(e) {
        e.preventDefault();
        let song = {
            title: $("#songTitle").val(),
            artist: $("#songArtist").val(),
            album: $("#songAlbum").val(),
            source: $("#songSource").val(),
            date: $("#showNight").val(),
            _token: urls._token
        }

        $.ajax({
            url: urls.addSong,
            data: song,
            method: "POST",
            success: function( res ) {
                $("#addSongForm input[type='text']").val('');
                dashboard.redraw();
            }
        });
        return false;
    },

    removeSong: function(e) {
        let id = $(this).data('id');
        $.ajax({
            url: urls.removeSong,
            data: {
                id: id,
                _token: urls._token
            },
            method: "POST",
            success: function() {
                dashboard.redraw();
            }
        })
    },

    reorder: function( e ) {
        let item = e.item;
        let index = e.newIndex;
        let oldIndex = e.oldIndex;

        let order = [];
        // get the new order
        $(".song").each(function() {
            order.push( $(this).attr('id') );
        })

        $.ajax({
            url: urls.updateQueuePosition,
            data: {
                _token: urls._token,
                song: $(item).attr('id'),
                order: order
            },
            method: "POST",
            success: function() {
                dashboard.redraw();
            }
        });
    },

    redraw: function() {
        $.ajax({
            url: urls.getQueue,
            method: "GET",
            success: function( res ) {
                $("#queue").html( res );
                minQueue = $(".song").first().data('queueposition');
                dashboard.setNextSong();
                dashboard.setCurrentSong();
                dashboard.initSortable();
            }
        })
    },

    setPositionZero: function() {
        if( dashboard.storage.getItem('queuePosition') == undefined ) {
            dashboard.queuePosition = minQueue - 1;
        } else {
            dashboard.queuePosition = parseInt(dashboard.storage.getItem('queuePosition'));
        }
        dashboard.setCurrentSong();
        dashboard.setNextSong();
    },

    setNextSong: function() {
        $(".song.next").removeClass('next');
        $(".song").filterByData('queueposition', dashboard.queuePosition + 1).addClass('next');
    },

    setCurrentSong: function() {
        $(".song.current").removeClass('current');
        $(".song").filterByData('queueposition', dashboard.queuePosition).addClass('current');

        dashboard.title = $(".current").data('title');
        dashboard.artist = $(".current").data('artist');
        dashboard.album = $(".current").data('album');
    },

    broadcaster: {

        socket: null,

        init: function() {
            dashboard.broadcaster.socket = io('142.93.5.178:3000');
        },

        setSong: function() {
            if( dashboard.title !== undefined ) {
                let msg = "Now Playing: " + dashboard.title + " by " + dashboard.artist;
                dashboard.broadcaster.socket.emit("set song", msg);
            } else {
                dashboard.broadcaster.clearMsg();
            }

        },

        clearMsg: function() {
            dashboard.broadcaster.socket.emit("clear");
        }
    }

}

dashboard.init();
